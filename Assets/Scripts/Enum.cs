﻿public enum GameState
{
    Start,
    Pause,
    Playing,
    GameClear,
    End,
    MainMenu,
    Jouer,
    Options,
    Quitter,
    Loading,
    LevelSelect,
}