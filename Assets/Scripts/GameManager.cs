﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class GameManager : MonoBehaviour
{
    public static GameState Status;
    public static GameObject MenuPrincipal, Settings, LevelSelect, Credits, TimerOn, TimerOff, MusicOn, MusicOff, Loading;
    public static GameManager current;
    public float time;
    public int NbPlayer, Count, NbLevel;
    public List<GameObject> Players;
    public EventSystem Event;
    AudioSource SE;
    public AudioClip MainMenu, IG;

    void Awake()
    {
        if (current == null)
        {
            current = this;
            DontDestroyOnLoad(this);
        }
        else if (current != this)
            Destroy(gameObject);
    }

    // Use this for initialization
    void Start ()
    {
        SE = GameObject.Find("Canvas1").GetComponent<AudioSource>();
        MenuPrincipal = GameObject.Find("MenuPrincipal");
        Settings = GameObject.Find("SettingsMenu");
        LevelSelect = GameObject.Find("LevelSelect");
        Credits = GameObject.Find("CreditsMenu");
        TimerOn = GameObject.Find("TimerOn");
        TimerOff = GameObject.Find("TimerOff");
        MusicOn = GameObject.Find("MusicOn");
        MusicOff = GameObject.Find("MusicOff");
        Loading = GameObject.Find("Loading");

        Settings.SetActive(false);
        Credits.SetActive(false);
        Loading.SetActive(false);
        LevelSelect.SetActive(false);

        if (Event.firstSelectedGameObject != GameObject.Find("Jouer"))
            Event.SetSelectedGameObject(GameObject.Find("Jouer"));
        Event.firstSelectedGameObject = GameObject.Find("Jouer");
    }
	
	// Update is called once per frame
	void Update ()
    {
        StartCoroutine("BGM");

        if (SceneManager.GetActiveScene().name != "MenuPrincipal")
            MenuPrincipal.SetActive(false);

        switch (Status)
        {
            case GameState.MainMenu:
                MenuPrincipal.SetActive(true);
                Settings.SetActive(false);
                Credits.SetActive(false);
                if (Event.firstSelectedGameObject != GameObject.Find("Jouer"))
                    Event.SetSelectedGameObject(GameObject.Find("Jouer"));
                Event.firstSelectedGameObject = GameObject.Find("Jouer");
                break;

            case GameState.Jouer:
                MenuPrincipal.SetActive(false);
                Settings.SetActive(true);
                StartCoroutine("SetData");
                if (Event.firstSelectedGameObject != GameObject.Find("3Joueurs"))
                    Event.SetSelectedGameObject(GameObject.Find("3Joueurs"));
                Event.firstSelectedGameObject = GameObject.Find("3Joueurs");
                break;

            case GameState.LevelSelect:
                Settings.SetActive(false);
                LevelSelect.SetActive(true);
                if (Event.firstSelectedGameObject != GameObject.Find("Level1"))
                    Event.SetSelectedGameObject(GameObject.Find("Level1"));
                Event.firstSelectedGameObject = GameObject.Find("Level1");
                break;

            case GameState.Options:
                MenuPrincipal.SetActive(false);
                Credits.SetActive(true);
                if (Event.firstSelectedGameObject != GameObject.Find("Retour"))
                    Event.SetSelectedGameObject(GameObject.Find("Retour"));
                Event.firstSelectedGameObject = GameObject.Find("Retour");
                break;

            case GameState.Quitter:
                Application.Quit();
                Debug.Log("Quit");
                break;

            case GameState.Playing:
                break;

            case GameState.GameClear:
                Players = new List<GameObject>(GameObject.FindGameObjectsWithTag("Player"));
                Players.Add(GameObject.FindGameObjectWithTag("Thief"));
                foreach (var item in Players)
                    item.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
                GameObject.Find("LevelManager").GetComponent<LevelManager>().CopWin();
                break;

            case GameState.End:
                Players = new List<GameObject>(GameObject.FindGameObjectsWithTag("Player"));
                Players.Add(GameObject.FindGameObjectWithTag("Thief"));
                foreach (var item in Players)
                    item.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
                GameObject.Find("LevelManager").GetComponent<LevelManager>().ThiefWin();
                break;
        }
	}

    IEnumerator SetData()
    {
        time = 60;
        NbPlayer = 3;
        Status = GameState.Playing;
        yield return new WaitForSeconds(0.1f);
    }

    public void OnClickBouton(int Number)
    {
        switch(Number)
        {
            case 0:
                Status = GameState.Jouer;
                break;

            case 1:
                Status = GameState.Options;
                break;

            case 2:
                Status = GameState.MainMenu;
                break;

            case 3:
                Status = GameState.Quitter;
                break;

            case 4:
                Status = GameState.LevelSelect;
                break;

            case 5:
                Loading.SetActive(true);
                MenuPrincipal.SetActive(true);
                Loading.GetComponent<LoadingScreen>().Loading(1);
                Status = GameState.Loading;
                break;
        }
    }

    public void OnClickToggle(int num)
    {
        switch (num)
        {
            case 3:
                NbPlayer = 3;
                break;

            case 4:
                NbPlayer = 4;
                break;
        }
    }

    public void OnClickDropdown(int num)
    {
        switch (GameObject.Find("TimeDeroulant").GetComponent<Dropdown>().value)
        {
            case 0:
                time = 60;
                break;

            case 1:
                time = 90;
                break;

            case 2:
                time = 120;
                break;
        }
    }

    public void OnClickLevel(int num)
    {
        switch (num)
        {
            case 1:
                NbLevel = num;
                Loading.SetActive(true);
                LevelSelect.SetActive(false);
                MenuPrincipal.SetActive(true);
                Loading.GetComponent<LoadingScreen>().Loading(0);
                Status = GameState.Loading;
                break;

            case 2:
                NbLevel = num;
                Loading.SetActive(true);
                LevelSelect.SetActive(false);
                MenuPrincipal.SetActive(true);
                Loading.GetComponent<LoadingScreen>().Loading(0);
                Status = GameState.Loading;
                break;

            case 3:
                NbLevel = num;
                Loading.SetActive(true);
                LevelSelect.SetActive(false);
                MenuPrincipal.SetActive(true);
                Loading.GetComponent<LoadingScreen>().Loading(0);
                Status = GameState.Loading;
                break;
        }
    }

    public void OnClickActiveTimer(int ActiveTimer)
    {
        switch(ActiveTimer)
        {
            case 0:
                TimerOn.SetActive(false);
                TimerOff.SetActive(true);
                break;

            case 1:
                TimerOn.SetActive(true);
                TimerOff.SetActive(false);
                break;
        }
    }

    public void OnClickActiveMusic(int ActiveMusic)
    {
        switch(ActiveMusic)
        {
            case 0:
                MusicOn.SetActive(false);
                MusicOff.SetActive(true);
                break;

            case 1:
                MusicOn.SetActive(true);
                MusicOff.SetActive(false);
                break;
        }
    }

    IEnumerator BGM()
    {
        if (SceneManager.GetActiveScene().name == "MenuPrincipal" && SE.clip != MainMenu)
        {
            SE.Stop();
            SE.clip = MainMenu;
            yield return new WaitForSeconds(0.2f);
            SE.Play();
        }
        else if (SceneManager.GetActiveScene().name == "Game" && SE.clip != IG)
        {
            SE.Stop();
            SE.clip = IG;
            yield return new WaitForSeconds(0.2f);
            SE.Play();
        }
    }
}
