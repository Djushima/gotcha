﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour {

    GameObject Win, Loose, Loading;
    AudioSource SE;
    public AudioClip thief, cop;

	// Use this for initialization
	void Start () {

        Win = GameObject.Find("CopWin");
        Loose = GameObject.Find("ThiefWin");
        Loading = GameObject.Find("Loading");
        SE = GameObject.Find("LevelManager").GetComponent<AudioSource>();

        Win.SetActive(false);
        Loose.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void CopWin()
    {
        StartCoroutine("Cop");
    }

    public void ThiefWin()
    {
        StartCoroutine("Thief");
    }

    IEnumerator Cop()
    {
        GameManager.Status = GameState.Playing;
        SE.clip = cop;
        SE.Play();
        yield return new WaitForSeconds(0.3f);
        Time.timeScale = 0;
        Win.SetActive(true);
        if (GameManager.current.Event.firstSelectedGameObject != GameObject.Find("Retry"))
            GameManager.current.Event.SetSelectedGameObject(GameObject.Find("Retry"));
        GameManager.current.Event.firstSelectedGameObject = GameObject.Find("Retry");
    }

    IEnumerator Thief()
    {
        GameManager.Status = GameState.Playing;
        SE.clip = thief;
        SE.Play();
        yield return new WaitForSeconds(0.2f);
        Time.timeScale = 0;
        Loose.SetActive(true);
        if (GameManager.current.Event.firstSelectedGameObject != GameObject.Find("Retry"))
            GameManager.current.Event.SetSelectedGameObject(GameObject.Find("Retry"));
        GameManager.current.Event.firstSelectedGameObject = GameObject.Find("Retry");
    }

    public void OnClickButton(int num)
    {
        switch (num)
        {
            case 0:
                Time.timeScale = 1;
                Loading.SetActive(true);
                Loading.GetComponent<LoadingScreen>().Loading(1);
                break;

            case 1:
                
                Loading.SetActive(true);
                Time.timeScale = 1;
                Loading.GetComponent<LoadingScreen>().Loading(0);
                break;
        }
    }
}
