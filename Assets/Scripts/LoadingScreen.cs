﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class LoadingScreen : MonoBehaviour {

    Scene scene;
    AsyncOperation Async;
    List<GameObject> Players;
    public System.Random rnd;
    public GameObject thief, P1, P2, P3;

    Vector2 Thief = new Vector2(0, 0);

    // Use this for initialization
    void Start () {
        int seed = Environment.TickCount;
        rnd = new System.Random(seed++);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Loading (int num)
    {
        scene = SceneManager.GetActiveScene();
        switch (scene.name)
        {
            case "MenuPrincipal":
                {
                    if (num == 0)
                        StartCoroutine("LoadGame");
                    else
                        StartCoroutine("LoadTuto");
                }
                break;

            case "Game":
                {
                    if (num == 0)
                        StartCoroutine("LoadMenu");
                    else
                        StartCoroutine("ReloadGame");
                }
                break;

            case "Tuto":
                {
                    StartCoroutine("LoadMenu");
                }
                break;
        }
    }


    IEnumerator LoadGame()
    {
        yield return new WaitForSecondsRealtime(1f);
        Async = SceneManager.LoadSceneAsync(1);
        while (!Async.isDone)
        {
            yield return null;
        }
        StartCoroutine("LevelData");
    }

    IEnumerator LoadTuto()
    {
        yield return new WaitForSecondsRealtime(1f);
        Async = SceneManager.LoadSceneAsync(2);
        while (!Async.isDone)
        {
            yield return null;
        }
    }

    IEnumerator LoadMenu()
    {
        yield return new WaitForSecondsRealtime(1f);
        Async = SceneManager.LoadSceneAsync(0);
        while (!Async.isDone)
        {
            yield return null;
        }
        yield return new WaitForSecondsRealtime(0.2f);
        GameManager.MenuPrincipal.SetActive(true);
        if (GameManager.current.Event.firstSelectedGameObject != GameObject.Find("Jouer"))
            GameManager.current.Event.SetSelectedGameObject(GameObject.Find("Jouer"));
        GameManager.current.Event.firstSelectedGameObject = GameObject.Find("Jouer");
        GameManager.Loading.SetActive(false);
        GameManager.Status = GameState.Playing;
    }

    IEnumerator ReloadGame()
    {
        yield return new WaitForSecondsRealtime(1f);
        Async = SceneManager.LoadSceneAsync(1);
        while (!Async.isDone)
        {
            yield return null;
        }
        StartCoroutine("LevelData");
    }

    IEnumerator LevelData()
    {
        yield return new WaitForSeconds(0.2f);
        Instantiate(Resources.Load<GameObject>("Level/Level" + GameManager.current.NbLevel));
        Instantiate(Resources.Load<GameObject>("Player/Player1"));
        Instantiate(Resources.Load<GameObject>("Player/Player2"));
        Instantiate(Resources.Load<GameObject>("Player/Player3"));
        if (GameManager.current.NbPlayer == 4)
            Instantiate(Resources.Load<GameObject>("Player/Player4"));
        Players = new List<GameObject>(GameObject.FindGameObjectsWithTag("Player"));
        thief = Players[rnd.Next(0, Players.Count)];
        thief.transform.position = Thief;
        GameManager.Loading.SetActive(false);
        switch (thief.name)
        {
            case "Player1(Clone)":
                thief.tag = "Thief";
                thief.GetComponent<Player1>().thief = true;
                thief.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Thief");
                break;

            case "Player2(Clone)":
                thief.tag = "Thief";
                thief.GetComponent<Player2>().thief = true;
                thief.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Thief");
                break;

            case "Player3(Clone)":
                thief.tag = "Thief";
                thief.GetComponent<Player3>().thief = true;
                thief.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Thief");
                break;

            case "Player4(Clone)":
                thief.tag = "Thief";
                thief.GetComponent<Player4>().thief = true;
                thief.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Thief");
                break;
        }
        GameManager.Status = GameState.Playing;
    }
}
