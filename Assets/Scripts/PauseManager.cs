﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PauseManager : MonoBehaviour {
    GameObject UIPause, Loading;
    
	// Use this for initialization
	void Start () {
        UIPause = GameObject.Find("UIPause");
        UIPause.SetActive(false);
        Loading = GameObject.Find("Loading");
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Joystick1Button7))
            {
            UIPause.SetActive(true);
            Time.timeScale = 0;
            if (GameManager.current.Event.firstSelectedGameObject != GameObject.Find("Reprendre"))
                GameManager.current.Event.SetSelectedGameObject(GameObject.Find("Reprendre"));
            GameManager.current.Event.firstSelectedGameObject = GameObject.Find("Reprendre");
        }
		
	}

    public void OnClickPozu(int pozu)
    {
        switch(pozu)
        {
            case 0:
                UIPause.SetActive(false);
                Time.timeScale = 1;
                break;
            case 1:
                UIPause.SetActive(false);
                Loading.SetActive(true);
                Time.timeScale = 1;
                Loading.GetComponent<LoadingScreen>().Loading(1);              
                break;
            case 2:
                UIPause.SetActive(false);
                Loading.SetActive(true);
                Time.timeScale = 1;
                Loading.GetComponent<LoadingScreen>().Loading(0);
                break;
            case 3:
                Application.Quit();
                break;
        }
    }
}
