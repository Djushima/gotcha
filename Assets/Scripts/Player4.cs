﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player4 : MonoBehaviour {

    Rigidbody2D m_rigidbody2D;
    AudioSource SE;

    int Speed = 5;
    public bool thief = false, End = false;

    void Awake()
    {
        m_rigidbody2D = GetComponent<Rigidbody2D>();
        SE = GetComponent<AudioSource>();
    }


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        //Récupération des Inputs
        float InputX = Input.GetAxis("Horizontal4");
        float InputY = Input.GetAxis("Vertical4");

        Move(InputX, InputY);
        if (GameManager.current.NbPlayer == 3)
        {
            if (GameManager.current.Count >= 2 && End != true)
            {
                GameManager.Status = GameState.GameClear;
                End = true;
            }
        }
        else
        {
            if (GameManager.current.Count >= 3 && End != true)
            {
                GameManager.Status = GameState.GameClear;
                End = true;
            }
        }

    }

    void Move(float X, float Y)
    {
        //Rotation du Sprite
        if (Mathf.Abs(X) > 0)
        {
            Quaternion rot = transform.rotation;
            transform.rotation = Quaternion.Euler(rot.x, rot.y, Mathf.Sign(X) == 1 ? -90 : 90);
        }
        if (Mathf.Abs(Y) > 0)
        {
            Quaternion rot = transform.rotation;
            transform.rotation = Quaternion.Euler(rot.x, rot.y, Mathf.Sign(Y) == 1 ? 0 : 180);
        }

        //Mouvements du Personnage
        if (X == 0 && Y == 0)
            Debug.Log("Nothing");
        else if (Mathf.Abs(X) > 0)
        {
            m_rigidbody2D.velocity = new Vector2(Speed * X, 0);
            //Anim Avance
        }
        else if (Mathf.Abs(Y) > 0)
        {
            m_rigidbody2D.velocity = new Vector2(0, Speed * Y);
            //Anim Monte
        }
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Thief")
        {
            GameManager.current.Count += 1;
            SE.Play();
            Debug.Log("+1");
        }
    }

    private void OnCollisionExit2D(Collision2D col)
    {
        if (col.gameObject.tag == "Thief")
        {
            GameManager.current.Count -= 1;
            Debug.Log("-1");
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Exit" && thief == true)
            GameManager.Status = GameState.End;
    }
}
