﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollingCredits : MonoBehaviour {
    float speed = 100f;
    // Use this for initialization
    void Start () {   
	}
	
	// Update is called once per frame
	void Update ()
    {
        transform.Translate(Vector3.up * Time.deltaTime * speed);   //défile vers le haut

        if (gameObject.transform.localPosition.y > 850)  //détruit l'objet quand il a atteint la bonne hauteur
        {
            GameManager.Status = GameState.MainMenu;
            GameManager.Status = GameState.Playing;
        }
            


        
    }
}
