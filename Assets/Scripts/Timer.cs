﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour {

    public bool end = false;
    Image timerBar;
    public float timeleft;
    public float maxTime;
    public Text timesUpText;
    public Text timeText;
    public List <GameObject> Sortie;
    AudioSource SE;
	// Use this for initialization

	void Start ()
    {
        SE = GameObject.Find("timerBar").GetComponent<AudioSource>();
        maxTime = GameManager.current.time;
        timesUpText.text = "";               //Texte vide au départ
        timerBar = this.GetComponent<Image>(); 
        timeleft = maxTime;       
    }
	
	// Update is called once per frame
	void Update () {
        if (end == false)                                                       //Tant que le timer et le feedback de warning n'est pas fini
            TimerClass();
        else
        {
            
            timeText.text = "/!/ Time : 0.0 /!/";                                //Fait clignoter le timer qui a atteint 0 jusqu'à la fin de la partie
            timeText.color = new Color(1, 0, 0, Mathf.PingPong(Time.time, 1));
            timerBar.color = new Color(1, 0, 0, Mathf.PingPong(Time.time, 1));
        }
	}

    void TimerClass()
    {
        if (timeleft > 0)                                                       //Si le timer est > 0, décrémente
        {
            timeleft -= Time.deltaTime;
            timerBar.fillAmount = timeleft / maxTime;
            timeText.text = "Time : " + timeleft.ToString("f1");                //Affichage d'une seule décimale
        }
        else
        {
            timesUpText.text = "/!/ Les portes de sorties se sont ouvertes ";                //Message d'avertissement lorsque le timer atteint 0
            StartCoroutine("Warning");
        }
    }

    IEnumerator Warning()      //Clignote 3 fois pendant 3 secondes & met la couleur du timer en rouge
    {
        Sortie = new List<GameObject>(GameObject.FindGameObjectsWithTag("Exit"));
        foreach (var sortie in Sortie)
        {
            sortie.GetComponent<SpriteRenderer>().sprite = Instantiate(Resources.Load<Sprite>("Sortie/patern sortie ouverte"));
            sortie.GetComponent<BoxCollider2D>().isTrigger = true;
        }
        SE.Play();
        timeText.color = new Color(1, 0, 0);
        timerBar.fillAmount = 100;
        timerBar.color = new Color(1, 0, 0);
        timesUpText.color = new Color (timesUpText.color.r, timesUpText.color.g, timesUpText.color.b, Mathf.PingPong(Time.time, 0.5f));
        yield return new WaitForSeconds(3f);
        timesUpText.text = "";
        
        end = true;
    }
}
