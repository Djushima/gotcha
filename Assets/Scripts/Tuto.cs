﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Tuto : MonoBehaviour {

    int screen = 1;
    bool change = true;
    public GameObject screen0;
    public GameObject screen1;
    public GameObject screen2;
    public GameObject screen3;
    public GameObject screen4;
    GameObject Loading;

    // Use this for initialization
    void Start () {
        Loading = GameObject.Find("Loading");
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetAxis("Submit") == 1 && change == true)
        {
            StartCoroutine("changescreen2");
        } 
	}

    public void changescreen()
    {
        switch(screen){
            
            case 1:
                screen0.SetActive(false);
                screen1.SetActive(true);
                screen = 2;
                break;
            case 2:
                screen1.SetActive(false);
                screen2.SetActive(true);
                screen = 3;
                break;
            case 3:
                screen2.SetActive(false);
                screen3.SetActive(true);
                screen = 4;
                break;
            case 4:
                screen3.SetActive(false);
                screen4.SetActive(true);
                screen = 5;
                break;
            default:
                Loading.SetActive(true);
                Loading.GetComponent<LoadingScreen>().Loading(0);
                break;

        }
    }

    IEnumerator changescreen2()
    {
        change = false;
        changescreen();
        yield return new WaitForSecondsRealtime(1);
        change = true;
    }
}
